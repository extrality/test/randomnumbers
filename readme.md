<!-- You can maintain a list, a buffer size, and write large amounts to disk at a time when the list fills up, then starting again with an empty list. This way, you're efficiently using your disk IO.

You may want to consider parallelizing this task as well, but that's a higher level concept you probably aren't comfortable with considering the domain of your question here. However the reason why is because you have web requests that are blocking processing while it waits for the response, when you could still be doing other things in the program. This requires the use of threads and since there's a single data target you'll have to manage resource access (great fit for a mutex lock). This would VASTLY improve performance (if you try doing this serially you will be making 10,000,000,000 requests x 0.01 sec/request, or 100,000,000 seconds. That's 1,157 days.)

Edit: also, this is assuming a pretty fast response time. It'll take so so so very long, seriously. -->

# https://docs.python.org/3/library/io.html
## Create a Stream for I/O
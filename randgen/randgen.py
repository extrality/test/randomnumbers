
from random import (
    randrange
)

def generateRandomNumbers(asked:int) -> list:
    """Generate a list of random numbers.

    Keyword arguments:
    asked -- the amount of numbers wanted (default 0)
    """

    integer_limit   = 100000000000000000000000000000000
    random_numbers  = []
    for cnt in range(0, asked):
        random_numbers.append(randrange(integer_limit))

    return random_numbers
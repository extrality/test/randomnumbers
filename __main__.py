# imports
try:
    # standart packages
    from datetime import datetime
    # custom packages
    # from dinput.dinput      import (

    # )
    # from order.order        import (

    # )
    # from reports.reports    import (
    #     reports
    # )
    from randgen.randgen    import (
        generateRandomNumbers,
    )
except Exception as exp:
    raise exp

def mainRoutine():
    # main routine
    start_time      = datetime.now()
    random_numbers  = generateRandomNumbers(1000000000)
    end_time        = datetime.now()
    exec_duration   = end_time - start_time

    # report
    print(f"Extrality Random Numbers Ordering")
    print(f"\t=> Program started\t:\t{start_time}")
    print(f"\t=> Random numbers asked\t:\t{len(random_numbers)}")
    print(f"\t=> Program ended\t:\t{end_time}")
    print(f"\t=> Execution duration\t:\t{exec_duration}")


if __name__ == "__main__":
    try:
        print(f"Launching mainRoutine()")
        mainRoutine()
    except Exception as exp:
        print(f"{exp}")